package wp

import (
	"context"
	"runtime"
	"sync"
	"sync/atomic"
	"time"
)


type JobError struct {
	job interface {}
	err error
}

type State struct {
	waiting int32
	processing int32
	completed int32
	errors int32
}

type Scheduler struct {
	sync.Mutex
	items chan interface{}
	backPressure []interface{}
	capacity int
	canceler context.CancelFunc
	state State
	results chan interface{}
	errors chan JobError
}

func NewScheduler(capacity int, handler func(interface {}) (interface{}, error)) *Scheduler {
	ctx, cancel := context.WithCancel(context.Background())

	state := State{}

	atomic.StoreInt32(&state.waiting, 0)
	atomic.StoreInt32(&state.processing, 0)
	atomic.StoreInt32(&state.completed, 0)
	atomic.StoreInt32(&state.errors, 0)

	scheduler := Scheduler{
		items: make(chan interface{}, capacity),
		backPressure: make([]interface{}, 0),
		capacity: capacity,
		canceler: cancel,
		state: state,
		results: make(chan interface{}, capacity),
		errors: make(chan JobError, capacity),
	}

	scheduler.initializeWorkers(ctx, handler)

	return &scheduler
}

func (s *Scheduler) initializeWorkers(ctx context.Context, handler func(interface {}) (interface{}, error)) {
	for i := 0; i < runtime.NumCPU(); i++ {
		go s.newWorker(ctx, handler)
	}
}

func (s *Scheduler) newWorker(ctx context.Context, handler func(interface {}) (interface{}, error)) {
	backoff := 0

	for {
		select {
		case <-ctx.Done():
			return
		case job := <- s.items:
			atomic.AddInt32(&s.state.waiting, -1)
			atomic.AddInt32(&s.state.processing, 1)

			job, err := handler(job)

			atomic.AddInt32(&s.state.processing, -1)

			if err != nil {
				atomic.AddInt32(&s.state.errors, 1)
				err := JobError{
					job,
					err,
				}
				select {
				case s.errors <- err:
				default:
				}
			} else {
				atomic.AddInt32(&s.state.completed, 1)
				select {
				case s.results <- job:
				default:
				}
			}

			backoff = 0

		default:
			backoff += 1
			s.CheckBackPressure()
			time.Sleep(time.Duration(backoff * 10) * time.Millisecond)
		}
	}
}

func (s *Scheduler) AddItem(item interface{}) {
	atomic.AddInt32(&s.state.waiting, 1)

	if len(s.items) < s.capacity {
		select {
		case s.items <- item:
			return
		}
	}

	s.Lock()
	defer s.Unlock()

	s.backPressure = append(s.backPressure, item)

	return
}

func (s *Scheduler) Process() {
	var wg sync.WaitGroup

	wg.Add(1)

	go func() {
		defer wg.Done()

		for {
			if atomic.LoadInt32(&s.state.waiting) == 0 && atomic.LoadInt32(&s.state.processing) == 0 {
				return
			}
			time.Sleep(50 * time.Millisecond)
		}
	}()

	wg.Wait()
}

func (s *Scheduler) CheckBackPressure() {
	s.Lock()
	defer s.Unlock()

	if len(s.backPressure) == 0 || s.capacity <= len(s.items) {
		return
	}

	job, tmp := s.backPressure[0], s.backPressure[1:]

	s.backPressure = tmp

	s.items <- job
	return
}

func (s *Scheduler) GetCompletedCount() int32 {
	return atomic.LoadInt32(&s.state.completed)
}

func (s *Scheduler) GetWaitingCount() int32 {
	return atomic.LoadInt32(&s.state.waiting)
}

func (s *Scheduler) GetProcessingCount() int32 {
	return atomic.LoadInt32(&s.state.processing)
}

func (s *Scheduler) GetErrorsCount() int32 {
	return atomic.LoadInt32(&s.state.errors)
}

func (s *Scheduler) GetResults() <-chan interface{} {
	return s.results
}

func (s *Scheduler) GetErrors() <-chan JobError {
	return s.errors
}

func (s *Scheduler) Stop() {
	s.canceler()
	close(s.items)
}
