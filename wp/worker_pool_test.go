package wp

import (
	"context"
	"errors"
	"runtime"
	"testing"
	"time"
)

type TestJob struct {
	Id int
	Status string
}

func TestCreatePool(t *testing.T) {
	handler := func (ptr interface {}) (interface {}, error) {
		job, ok := (ptr).(*TestJob)

		if ok != true {
			return ptr, errors.New("failed to convert job")
		}

		job.Status = "Done"

		return job, nil
	}

	scheduler := NewScheduler(5, handler)

	ctx, cancel := context.WithCancel(context.Background())

	results := make([]interface{}, 0)

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case job := <-scheduler.GetResults():
				results = append(results, job)
			}
		}
	}()

	scheduler.AddItem(&(TestJob {
		Id: 1,
		Status: "New",
	}))

	scheduler.Process()

	cancel()

	scheduler.Stop()

	runtime.Gosched()

	if scheduler.GetWaitingCount() != 0 {
		t.Error("there should not be any jobs still waiting")
	}

	if scheduler.GetProcessingCount() != 0 {
		t.Error("there should not be any jobs still processing")
	}

	if scheduler.GetErrorsCount() != 0 {
		t.Error("there should not be any failed jobs")
	}

	if scheduler.GetCompletedCount() != 1 {
		t.Error("there should be one job result")
	}

	if len(results) != 1 {
		t.Fatal("the results should contain the job item")
	}

	job, ok := (results[0]).(*TestJob)

	if ok == false {
		t.Fatal("failed to convert job back to TestJob")
	}

	if job.Status != "Done" {
		t.Fatal("failed to modify job when passed to the handler")
	}


}

func TestStop(t *testing.T) {
	didRun := false

	handler := func (ptr interface {}) (interface {}, error) {
		time.Sleep(60 * time.Second)

		didRun = true
		return ptr, nil
	}

	scheduler := NewScheduler(5, handler)

	scheduler.AddItem(struct {}{})

	scheduler.Stop()

	if didRun == true {
		t.Fatal("didRun should not be true - failed to cancel correctly")
	}
}



func TestExceedingCapacity(t *testing.T) {

	handler := func (ptr interface{}) (interface{}, error) {
		job, ok := (ptr).(*TestJob)

		if ok != true {
			return nil, errors.New("failed to convert job")
		}

		// simulate work
		time.Sleep(50 * time.Millisecond)

		return job, nil
	}

	scheduler := NewScheduler(5, handler)

	for i := 0; i < 25; i++ {
		scheduler.AddItem(&(TestJob { Id: i }))
	}

	scheduler.Process()
	scheduler.Stop()

	if scheduler.GetWaitingCount() != 0 {
		t.Error("there should not be any jobs still waiting")
	}

	if scheduler.GetProcessingCount() != 0 {
		t.Error("there should not be any jobs still processing")
	}

	if scheduler.GetErrorsCount() != 0 {
		t.Error("there should not be any failed jobs")
	}

	if scheduler.GetCompletedCount() != 25 {
		t.Error("there should be one job result")
	}
}


