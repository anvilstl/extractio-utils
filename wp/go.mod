module bitbucket.org/anvilstl/extractio-utils/wp

go 1.12

require (
	github.com/labstack/gommon v0.2.8
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/valyala/fasttemplate v1.0.1 // indirect
)
